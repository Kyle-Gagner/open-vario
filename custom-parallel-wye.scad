// Copyright © 2024 Kyle Gagner

include <junction-track.scad>
include <quality.scad>

fraction = 0.66667; // [0.16667 : 0.083333 : 2]
conn_northwest = 1; // [-1 : 1]
conn_northeast = 1; // [-1 : 1]
conn_south = -1; // [-1 : 1]
top_surface = "rails"; // ["rails", "road", "flat"]
bottom_surface = "rails"; // ["rails", "road", "flat"]
tactile = false;
count = 1; // [1 : 2]

vario_parallel_wye(
    fraction = fraction,
    count = count,
    conn_northwest = conn_northwest,
    conn_northeast = conn_northeast,
    conn_south = conn_south,
    $vario_profile_top_rails = top_surface == "rails",
    $vario_profile_bottom_rails = bottom_surface == "rails",
    $vario_profile_top_road = top_surface == "road",
    $vario_profile_bottom_road = bottom_surface == "road");