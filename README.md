---
Copyright © 2024 Kyle Gagner
---

# Open-Vario

Open-Vario is an open source design for toy train track. Intercompatible track is manufactured in wood and plastic by many brands, but there is no industry standard design specification for this toy system&dagger;, sometimes called the vario system. Open-Vario's OpenSCAD files specify dimensions, implement design, and are ready to use for 3D printing and other applications.

&dagger; according to author's understanding

Open-Vario is in active development. No part of the design is finalized at this time. If you have ideas, requests, or bug reports for Open-Vario, send an E-mail to <kyle@gagner.us>. Please prefix the subject line `Open-Vario:`.

## Releases

### 2025.01

Gitlab: <https://gitlab.com/Kyle-Gagner/open-vario/-/tree/release/2025.01?ref_type=heads>

Thingiverse: <https://www.thingiverse.com/thing:6895688>

## Usage

### Pre-Rendered Files

A selection of pre-rendered files will are available on Thingiverse, including one STL of each track piece and a PDF draft of track plans and profiles. These are good for evaluating and getting started with Open-Vario.

### Customizer Scripts

The scripts like _custom-straight-track.scad_ and other _custom-*.scad_ files are designed for [OpenSCAD's customizer](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Customizer). Customized track is easy to use for 3D printing. See [Installation](#installation) below to obtain the files.

### Dimensions Specification

The top of _open-vario.scad_ contains dimensions specifications as commented global variables. A visual aid for interpreting the specification is provided in _draft.scad_. OpenSCAD can render and [export](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Export) the draft to PDF &mdash; a background of 2.5mm grid is recommended. See [Installation](#installation) below to obtain the files. The draft PDF is also available in [Pre-Rendered Files](#pre-rendered-files).

### OpenSCAD Library

All global variables and primitive parts are in _open-vario.scad_. To use Open-Vario as an OpenSCAD library, put `include <open-vario/open-vario.scad>` at the top of your script. To use complex parts, you may also include _tactile-track.scad_ and/or _junction-track.scad_. You may include _quality.scad_ to use the same quality defaults used in the customizer scripts. See [Installation](#installation) below to install Open-Vario into your OpenSCAD installation, or to add it as a Git submodule in your own repository. See [API Docs](#api-docs) below for library usage.

## Installation

You can obtain Open-Vario as a zip archive or clone it using [Git](https://git-scm.com/downloads).

* Latest zip: <https://gitlab.com/Kyle-Gagner/open-vario/-/archive/main/open-vario-main.zip>
* Clone latest:
  ```
  https://gitlab.com/Kyle-Gagner/open-vario.git
  ```
* \[Optional\] If using as a library, install in an OpenSCAD library path:
  * Refer to: [Open-SCAD User Manual/Libraries](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Libraries#Library_locations)
  * Place the following scripts in an _open-vario/_ subdirectory of your library location:
    * _open-vario.scad_
    * _tactile-track.scad_
    * _junction-track.scad_
* \[Optional\] If creating your own repository, consider adding Open-Vario as a [Git submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

## API Docs

The primitive modules in _open-vario.scad_ and complex parts in _tactile-track.scad_ and _junction-track.scad_ are parameterized with common parameters and special variables. See also [Dimensions Specification](#dimensions-specification) above for information on global variables which may be used by your scripts.

### Common Parameters

Common parameters have the same purpose in multiple modules

| Parameter | Purpose | Recommendation |
| :--- | :--- | :--- |
| count | number of parallel tracks | 1 or 2 |
| z1, z2 | Z-axis extent of connector pegs | defaults except special cases |
| fraction | length of a track piece as a fraction of vario_basic_length | 1/4, 1/3, 1/2, 2/3, 3/4, 1 |
| length | override fraction with a specified length | as required, limited uses |
| number | curve radius is `vario_basic_radius + vario_track_spacing*(number + (count - 1)/2)` | 0 through 3 |
| radius | override number with a specified radius | as required, limited uses |
| angle | angle of a curve | 15, 30, 45, 60 |
| shift | for sigmoid track, the horizontal (X-axis) distance between centers at either end of the track as multiples of vario_track_spacing | -2 through 2 in steps of 1/2 |
| conn_* | connector polarities at track ends | &gt;0 (peg), =0 (none), &lt;0 (hole) |
| extend | create track primitives with slightly extended ends | `true` for negative track profiles when constructing with combined primitives |
| symmetric | for select track pieces specifies whether the part is symmetric | `true` or `false` |

### Profile Special Variables

These special variables shape the profile of track and are `undef` by default. Setting these special variables overrides default behaviors. Road on a top or bottom surface always overrides rails on the same surface.

| Special Variable | Purpose | Overrides |
| :--- | :--- | :--- |
| $vario_profile_chamfer_rail | set `false` to produce a profile with no chamfers on the edges of its rails and/or road surfaces |
| $vario_profile_taper_rail | set `false` to produce a profile with no taper to the edges of its rails and/or road surfaces |
| $vario_profile_positive | set `true` to produce a profile with no rails or road surface | $vario_profile_\*[rails\|rail\|road\] |
| $vario_profile_negative | set `true` to produce a negative profile consisting only of rails and/or road cutouts | |
| $vario_profile_rails | set `false` to produce a profile with no rails | |
| $vario_profile_\[top\|bottom\]_rails | sets top or bottom surface rails | $vario_profile_rails |
| $vario_profile_\[top\|bottom\]_\[left\|right\]_rail | sets specific rails | $vario_profile_rails, $vario_profile_\[top\|bottom\]_rails |
| $vario_profile_roads | set `true` to produce a profile with roads | |
| $vario_profile_\[top\|bottom\]_road | sets top or bottom surface road | $vario_profile_roads, $vario_profile_\[top\|bottom\]_road |
| $vario_profile_chamfers | set `false` to produce a profile with no track edge chamfers |
| $vario_profile_\[top\|bottom\]_chamfers | sets top or bottom edge chamfers | $vario_profile_chamfers |
$vario_profile_\[top\|bottom\]_\[left\|right\]_chamfer | sets specific chamfers | $vario_profile_chamfers, $vario_profile_\[top\|bottom\]_chamfers |
| $vario_profile_full_width | set `true` to make track in _vario_track_spacing_ width rather than _vario_track_width_ |

### Plan and Profile Modules

Nearly all shapes in Open-Vario are combined extrusions from these fundamental modules:

| Module | Purpose |
| :--- | :--- |
| vario_rail_negative, vario_road_negative | for construction of track profiles |
| vario_chamfer_negative | for construction of track profiles |
| vario_connector_positive_plan | for extrusion into a track peg end |
| vario_connector_negative_plan | for extrusion into a hole (peg receptacle) track end negative |
| vario_track_profile | for extrusion into a track |

### Connector Solid Modules

| Module | Purpose |
| :--- | :--- |
| vario_connector_positive | to be unioned to a track end for a peg |
| vario_connector_negative | to be subtracted from a track end for a hole |
| vario_connector | to be unioned and subtracted to/from a track end, for parameterized connections in a track module |

To use vario_connector, union the track end with negative `false` and difference from the track end with negative `true`. By passing the polarity through in each case, the module will automatically handle unioning the peg, subtracting the hole, or neither unioning nor subtracting, as appropriate for the polarity.

### Primitive Track Modules

| Module | Remarks |
| :--- | :--- |
| vario_straight_track | straight track |
| vario_curve_track | positive angles curve to the right |
| vario_sigmoid_track | positive shifts move to the right |
| vario_parabola_track | primitive for future hill track features |

### Complex Track Modules

Complex track pieces are combined from primitives. Complex track itself is not intended to be combined in assembling yet further derived track &mdash; these are merely parametric track pieces. Modules are found in _tactile-track.scad_ and _junction-track.scad_.

# Licensing

The MIT license is permissive, easy to use, and easy to understand. However, Thingiverse does not offer the option of MIT licensing. For that reason, this work is dual licensed under the MIT license and the BSD-2-Clause license. That is, you may exercise your preference between the two.