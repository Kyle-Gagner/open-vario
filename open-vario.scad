// Copyright © 2024 Kyle Gagner

$eps                = 0.01; // small offset

// essential, de-facto standard dimensions of the vario system
vario_rail_width    = 6;    // width of rail groove
vario_rail_depth    = 3;    // depth of rail groove
vario_rail_centers  = 25;   // center to center spacing of rails
vario_track_width   = 40;   // overall width of track
vario_track_height  = 12;   // height of track
vario_track_spacing = 45;   // center to center spacing of parallel tracks

// essential dimensions of the vario system as recommended by Open-Vario to be compatible with popular track
vario_peg_diameter  = 11.5; // diameter of the connecting peg
vario_peg_offset    = 11.5; // offset of the center of the circle from the end of the track
vario_peg_neck      = 6.125;// width of the neck connecting the end of the track to the peg's circle
vario_peg_kerf      = 0.875;// the peg's kerf, as if cut by a jigsaw, widens peg of negative connector and neck by half

// Open-Vario's dimensions designed for consistency with popular track; these are less critical but can affect function
vario_peg_bevel     = 1;    // width and height of the bevel on the negative connector
vario_rail_taper    = 15;   // angle of each sidewall of the tapered rail
vario_rail_chamfer  = 1;    // chamfer on either side of the rail
vario_track_chamfer = 1.5;  // chamfer of a track piece's edges

// Open-Vario's track system design, designed to improve on and maintain high compatibility with common dimensions
vario_basic_length  = 216;  // full length straight track recommended in 1/4, 1/3, 1/2, 2/3, and 3/4 of basic length
vario_basic_radius  = 108;  // curves recommended in basic radius plus multiples of track spacing, in 30 and 45 degrees

// Open-Vario's hill design recommends parabolic hill base and crest pieces with straight track on the inclines
vario_basic_rise    = 72;   // recommended floor to overpass level
vario_basic_grade   = 0.25; // recommended hill grade (25%)
vario_basic_osc_r   = 288;  // parabolas with multiples of basic osculating circle radius at 0% grade point recommended

// profile that a single rail cuts out of a track piece (origin is centered at the top of the cut)
module vario_rail_negative()
{
    chamfer = is_undef($vario_profile_chamfer_rail) ? true : $vario_profile_chamfer_rail;
    taper = is_undef($vario_profile_taper_rail) ? true : $vario_profile_taper_rail;
    x1 = (chamfer ? vario_rail_chamfer : 0) +
        (vario_rail_width + (taper ? sin(vario_rail_taper)*vario_rail_depth : 0))/2;
    x2 = (vario_rail_width - (taper ? sin(vario_rail_taper)*vario_rail_depth : 0))/2;
    cr = chamfer ? vario_rail_chamfer : 0;
    da = $fn == 0 ? max($fa, cr == 0 ? 0 : $fs*180/PI/cr) : 360/$fn;
    av = (90 - (taper ? vario_rail_taper : 0))/da < 2 ?
        [90, taper ? vario_rail_taper : 0] :
        [for (a = [90 : -da : taper ? vario_rail_taper : 0]) a];
    polygon([
        [-x1, $eps],
        for (angle = true ? av : [0 : 0])
            [-x1 + cos(angle)*cr, -cr + sin(angle)*cr],
        [-x2, -vario_rail_depth],
        [x2, -vario_rail_depth],
        for (angle = true ? [for (i = [1 : len(av)]) av[len(av) - i]] : [0 : 0])
            [x1 - cos(angle)*cr, -cr + sin(angle)*cr],
        [x1, $eps]]);
}

// profile that a road cuts out of a track piece (origin is centered at the top of the cut)
module vario_road_negative()
{
    translate([-vario_rail_centers/2, 0])
        vario_rail_negative();
    translate([vario_rail_centers/2, 0])
        vario_rail_negative();
    translate([-vario_rail_centers/2, -vario_rail_depth])
        square([vario_rail_centers, vario_rail_depth + $eps]); 
}

// profile that a single chamfer cuts out of the edge of a track piece (upper right edge, origin is on the discarded edge)
module vario_chamfer_negative()
{
    da = $fn == 0 ? max($fa, $fs*180/PI/vario_track_chamfer) : 360/$fn;
    polygon([
        [-vario_track_chamfer, $eps],
        for (angle = [90 : -da : 0])
            [(cos(angle) - 1)*vario_track_chamfer, (sin(angle) - 1)*vario_track_chamfer],
        [$eps, -vario_track_chamfer],
        [$eps, $eps]]);
}

// plan of a positive connector (origin is centered at the root of the connector)
module vario_connector_positive_plan()
{
    translate([-vario_peg_neck/2, -$eps])
        square([vario_peg_neck, vario_peg_offset]);
    translate([0, vario_peg_offset])
        circle(vario_peg_diameter/2);
}

// plan of a negative connector (origin is centered at the root of the connector)
module vario_connector_negative_plan()
{
    translate([-vario_peg_neck/2 - vario_peg_kerf/2, -$eps])
        square([vario_peg_neck + vario_peg_kerf, vario_peg_offset]);
    translate([0, vario_peg_offset])
        circle(vario_peg_diameter/2 + vario_peg_kerf);
    translate([0, -$eps])
        rotate(45)
            square((vario_peg_neck + 2*vario_peg_kerf + 2*vario_peg_bevel)/sqrt(2), center = true);
}

// positive connector (origin is centered at the root of the connector)
module vario_connector_positive(count = 1, z1 = -vario_track_height/2, z2 = vario_track_height/2)
{
    for (i = [0 : count - 1])
        translate([vario_track_spacing*(i - (count - 1)/2), 0, 0])
            render()
                intersection()
                {
                    translate([0, 0, z1]) 
                        linear_extrude(z2 - z1, convexity = 2*count)
                            vario_connector_positive_plan();
                    rotate([90, 0, 180])
                        linear_extrude(
                            vario_peg_diameter + 2*vario_peg_offset,
                            convexity = 3, center = true)
                            vario_track_profile(count);
                }
}

// negative connector (origin is centered at the root of the connector)
module vario_connector_negative(count = 1, z1 = -vario_track_height/2, z2 = vario_track_height/2)
{
    translate([0, 0, z1 - $eps])
        linear_extrude(z2 - z1 + 2*$eps, convexity = 2*count)
            for (i = [0 : count - 1])
                translate([vario_track_spacing*(i - (count - 1)/2), 0, 0]) 
                    vario_connector_negative_plan();
}

module vario_connector(
    count = 1, polarity = 1, negative = false,
    z1 = -vario_track_height/2, z2 = vario_track_height/2)
{
    peg_positive = is_undef($vario_peg_positive) ? false : $vario_peg_positive;
    peg_negative = is_undef($vario_peg_negative) ? false : $vario_peg_negative;
    if (negative && polarity < 0 && !peg_positive && !peg_negative)
        vario_connector_negative(count = count, z1 = z1, z2 = z2);
    else if (!negative && polarity < 0 && peg_negative)
        rotate([0, 0, 180])
            vario_connector_negative(count = count, z1 = z1, z2 = z2);
    else if (!negative && polarity > 0 && !peg_negative)
        vario_connector_positive(count = count, z1 = z1, z2 = z2);
}

// profile of a track (origin is centered at the bottom of the track)
module vario_track_profile(count = 1)
{
    positive =!(is_undef($vario_profile_positive) || !$vario_profile_positive);
    negative = !(is_undef($vario_profile_negative) || !$vario_profile_negative);
    roads = is_undef($vario_profile_roads) ? false : $vario_profile_roads;
    top_road = !positive && (is_undef($vario_profile_top_road) ? roads : $vario_profile_top_road);
    bottom_road = !positive && (is_undef($vario_profile_bottom_rails) ? roads : $vario_profile_bottom_road);
    rails = is_undef($vario_profile_rails) ? true : $vario_profile_rails;
    top_rails = is_undef($vario_profile_top_rails) ? rails : $vario_profile_top_rails;
    bottom_rails = is_undef($vario_profile_bottom_rails) ? rails : $vario_profile_bottom_rails;
    top_left_rail = !positive && !top_road &&
        (is_undef($vario_profile_top_left_rail) ? top_rails : $vario_profile_top_left_rail);
    top_right_rail = !positive && !top_road &&
        (is_undef($vario_profile_top_right_rail) ? top_rails : $vario_profile_top_right_rail);
    bottom_left_rail = !positive && !bottom_road &&
        (is_undef($vario_profile_bottom_left_rail) ? bottom_rails : $vario_profile_bottom_left_rail);
    bottom_right_rail = !positive && !bottom_road &&
        (is_undef($vario_profile_bottom_right_rail) ? bottom_rails : $vario_profile_bottom_right_rail);
    chamfers = is_undef($vario_profile_chamfers) ? true : $vario_profile_chamfers;
    top_chamfers = is_undef($vario_profile_top_chamfers) ? chamfers : $vario_profile_top_chamfers;
    bottom_chamfers = is_undef($vario_profile_bottom_chamfers) ? chamfers : $vario_profile_bottom_chamfers;
    top_left_chamfer = !negative &&
        (is_undef($vario_profile_top_left_chamfer) ? top_chamfers : $vario_profile_top_left_chamfer);
    top_right_chamfer = !negative &&
        (is_undef($vario_profile_top_right_chamfer) ? top_chamfers : $vario_profile_top_right_chamfer);
    bottom_left_chamfer = !negative &&
        (is_undef($vario_profile_bottom_left_chamfer) ? bottom_chamfers : $vario_profile_bottom_left_chamfer);
    bottom_right_chamfer = !negative &&
        (is_undef($vario_profile_bottom_right_chamfer) ? bottom_chamfers : $vario_profile_bottom_right_chamfer);
    full_width = is_undef($vario_profile_full_width) ? false : $vario_profile_full_width;
    width = full_width ? vario_track_spacing*count : (count - 1)*vario_track_spacing + vario_track_width;
    difference()
    {
        if (!negative)
            square([width, vario_track_height], center = true);
        union()
        {
            
            for (i = [0 : count - 1])
                translate([vario_track_spacing*(i - (count - 1)/2), 0, 0]) 
                {
                    if (top_left_rail)
                        translate([-vario_rail_centers/2, vario_track_height/2])
                            vario_rail_negative();
                    if (top_right_rail)
                        translate([vario_rail_centers/2, vario_track_height/2])
                            vario_rail_negative();
                    if (bottom_left_rail)
                        translate([-vario_rail_centers/2, -vario_track_height/2])
                            mirror([0, 1])
                                vario_rail_negative();
                    if (bottom_right_rail)
                        translate([vario_rail_centers/2, -vario_track_height/2])
                            mirror([0, 1])
                                vario_rail_negative();
                    if (top_road)
                        translate([0, vario_track_height/2])
                            vario_road_negative();
                    if (bottom_road)
                        translate([0, -vario_track_height/2])
                            mirror([0, 1])
                                vario_road_negative();
                }
            if (top_left_chamfer)
                translate([-width/2, vario_track_height/2])
                    mirror([1, 0])
                        vario_chamfer_negative();
            if (bottom_left_chamfer)
                translate([-width/2, -vario_track_height/2])
                    mirror([1, 1])
                        vario_chamfer_negative();
            if (top_right_chamfer)
                translate([width/2, vario_track_height/2])
                    vario_chamfer_negative();
            if (bottom_right_chamfer)
                translate([width/2, -vario_track_height/2])
                    mirror([0, 1])
                        vario_chamfer_negative();
        }
    }
}

// straight track
module vario_straight_track(
    fraction = 1, count = 1,
    conn_north = 1, conn_south = -1,
    length = undef, extend = false)
{
    l = (is_undef(length) ? fraction*vario_basic_length : length) + (extend ? 2*$eps : 0);
    translate([0, extend ? -$eps : 0, 0]) 
        difference()
        {
            union()
            {
                rotate([-90, 0, 0])
                    mirror([0, 1, 0]) 
                        linear_extrude(l, convexity = 3)
                            vario_track_profile(count);
                translate([0, l, 0])
                    vario_connector(count = count, polarity = conn_north);
                rotate([0, 0, 180]) 
                    vario_connector(count = count, polarity = conn_south);
            }
            translate([0, l, 0])
                rotate([0, 0, 180]) 
                    vario_connector(count = count, polarity = conn_north, negative = true);
            vario_connector(count = count, polarity = conn_south, negative = true);
        }
}

// curved track
module vario_curve_track(
    number = 2, count = 1, angle = 45,
    conn_north = 1, conn_south = -1,
    radius = undef, extend = false)
{
    r = is_undef(radius) ? vario_basic_radius + vario_track_spacing*(number + (count - 1)/2) : radius;
    translate([sign(angle)*r, 0, 0])
    {
        difference()
        {
            union()
            {
                scale([1, -sign(angle), 1]) 
                    rotate([0, 0, extend ? -$eps : 0]) 
                        rotate_extrude(angle = abs(angle) + (extend ? 2*$eps : 0), convexity = 5)
                            translate([-sign(angle)*r, 0])
                                vario_track_profile(count);
                rotate([0, 0, -angle])
                    translate([-sign(angle)*r, 0, 0])
                        vario_connector(count = count, polarity = conn_north);
                translate([-sign(angle)*r, 0, 0])
                    rotate([0, 0, 180]) 
                        vario_connector(count = count, polarity = conn_south);
            }
            rotate([0, 0, -angle])
                translate([-sign(angle)*r, 0, 0])
                    rotate([0, 0, 180]) 
                        vario_connector(count = count, polarity = conn_north, negative = true);
            translate([-sign(angle)*r, 0, 0])
                vario_connector(count = count, polarity = conn_south, negative = true);
        }
    }
}

// vertical curve track (crest or base of hill)
module vario_parabola_track(
    number = 1, count = 1,
    grade1 = 0, grade2 = vario_basic_grade,
    conn_north = 1, conn_south = -1,
    osc_r = undef)
{
    r = is_undef(osc_r) ? vario_basic_osc_r*(number + 1) : osc_r;
    a = sign(grade2 - grade1)/r/2;
    l = abs((grade2 - grade1)/(2*a));
    b = grade1;
    c = -(grade1 < 0 && grade2 > 0 ? -b^2/(4*a) : min(0, a*l^2 + b*l));
    pieces = min(max($fn, abs(ceil(l/r/(PI*$fa/180)))), ceil(l/$fs));
    pad = vario_track_height;
    difference()
    {
        union()
        {
            render(convexity = 6)
                for (i = [0 : pieces - 1])
                    let(x1 = l*i/pieces, x2 = l*(i + 1)/pieces,
                        y1 = a*x1^2 + b*x1 + c, y2 = a*x2^2 + b*x2 + c,
                        g1 = 2*a*x1 + b, g2 = 2*a*x2 + b,
                        c1 = 1/(1 + g1^2), c2 = 1/(1 + g2^2),
                        s1 = g1*c1, s2 = g2*c2)
                            intersection()
                            {
                                rotate([90, 0, 90])
                                    linear_extrude(vario_track_spacing*count, center = true) 
                                        polygon([
                                            [x1 + pad*s1 - $eps*c1, y1 - pad*c1 - $eps*s1],
                                            [x1 - pad*s1 - $eps*c1, y1 + pad*c1 - $eps*s1],
                                            [x2 - pad*s2 + $eps*c2, y2 + pad*c2 + $eps*s2],
                                            [x2 + pad*s2 + $eps*c2, y2 - pad*c2 + $eps*s2]]);
                                translate([0, x1, y1])
                                    rotate([atan2(y2 - y1, x2 - x1), 0, 0])
                                        translate([0, -pad, 0])
                                            vario_straight_track(
                                                length = sqrt((y2 - y1)^2 + (x2 - x1)^2) + 2*pad,
                                                conn_north = 0, conn_south = 0);
                            }
            translate([0, l, a*l^2 + b*l + c])
                rotate([atan(grade2), 0, 0])
                    vario_connector(count = count, polarity = conn_north);
            translate([0, 0, c])
                rotate([0, 0, 180])
                    rotate([-atan(grade1), 0, 0])
                        vario_connector(count = count, polarity = conn_south);
        }
        translate([0, l, a*l^2 + b*l + c])
            rotate([0, 0, 180])
                rotate([-atan(grade2), 0, 0])
                    vario_connector(
                        count = count, polarity = conn_north, negative = true,
                        z1 = -vario_track_height, z2 = vario_track_height);
        translate([0, 0, c])
            rotate([atan(grade1), 0, 0])
                vario_connector(
                    count = count, polarity = conn_south, negative = true,
                    z1 = -vario_track_height, z2 = vario_track_height);
    }
}

module vario_sigmoid_track(
    fraction = 2/3, count = 1, shift = 1,
    conn_north = 1, conn_south = -1,
    length = undef, extend = false)
{
    if (shift == 0)
        vario_straight_track(
            fraction = fraction, count = count,
            conn_north = conn_north, conn_south = conn_south,
            length = length, extend = extend);
    else
    {
        l = (is_undef(length) ? fraction*vario_basic_length : length) + (extend ? 2*$eps : 0);
        angle = 2*atan2(shift*vario_track_spacing, l);
        r = abs(l/2/sin(angle));
        vario_curve_track(
            count = count, angle = angle + sign(angle)*$eps,
            conn_north = 0, conn_south = conn_south,
            radius = r, extend = extend);
        translate([shift*vario_track_spacing, l, 0])
            rotate([0, 0, 180]) 
                vario_curve_track(
                    count = count, angle = angle,
                    conn_north = 0, conn_south = conn_north,
                    radius = r, extend = extend);
    }
}
