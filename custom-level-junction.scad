// Copyright © 2024 Kyle Gagner

include <junction-track.scad>
include <quality.scad>

fraction = 0.33333; // [0.33333 : 0.083333 : 1]
angle = 90; // 15
conn_north = 1; // [-1 : 1]
conn_south = -1; // [-1 : 1]
top_surface = "rails"; // ["rails", "road", "flat"]
bottom_surface = "rails"; // ["rails", "road", "flat"]
count = 1; // [1 : 2]

vario_level_junction(
    fraction = fraction,
    count = count,
    angle = angle,
    conn_north = conn_north,
    conn_south = conn_south,
    $vario_profile_top_rails = top_surface == "rails",
    $vario_profile_bottom_rails = bottom_surface == "rails",
    $vario_profile_top_road = top_surface == "road",
    $vario_profile_bottom_road = bottom_surface == "road");