// Copyright © 2024 Kyle Gagner

include <junction-track.scad>
include <quality.scad>

fraction = 0.66667; // [0.5 : 0.083333 : 1]
conn_north = 1; // [-1 : 1]
conn_northeast = 1; // [-1 : 1]
conn_south = -1; // [-1 : 1]
top_surface = "rails"; // ["rails", "road", "flat"]
bottom_surface = "rails"; // ["rails", "road", "flat"]
count = 1; // [1 : 2]
symmetric = false;

vario_parallel_switch(
    fraction = fraction,
    count = count,
    conn_north = conn_north,
    conn_northeast = conn_northeast,
    conn_south = conn_south,
    symmetric = symmetric,
    $vario_profile_top_rails = top_surface == "rails",
    $vario_profile_bottom_rails = bottom_surface == "rails",
    $vario_profile_top_road = top_surface == "road",
    $vario_profile_bottom_road = bottom_surface == "road");