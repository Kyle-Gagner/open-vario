// Copyright © 2024 Kyle Gagner

include <open-vario.scad>
include <quality.scad>

tearaway_size = 5;

module tearaway(width)
{
    polygon([
        for (x = [-width/2 : tearaway_size : width/2])
            [x*(width + 2*$eps)/width, abs((x + width) % (tearaway_size * 2))],
        [width/2 + $eps, -$eps],
        [-width/2 - $eps, -$eps]]);
}

module parts()
{
    $vario_profile_taper_rail = false;
    $vario_profile_chamfer_rail = false;
    $vario_profile_chamfers = false;
    vario_straight_track(fraction = 1/6, conn_south = 0);
        translate([0, vario_basic_length/6 + $eps, 0])
            vario_straight_track(fraction = 1/6, conn_north = 0);
        translate([vario_track_spacing*1.5, 0, 0])
            vario_curve_track(number = 0, angle = -45);
        translate([vario_track_spacing*2.5, 0, 0])
            vario_curve_track(number = 1, angle = -45);
}

translate([0, 0, 0])
{
    translate([0, 10, 0])
        difference()
        {
            projection()
                parts();
            difference()
            {
                projection()
                    parts($vario_profile_negative = true);
                offset(delta = -$eps)
                    projection()
                        parts($vario_profile_negative = true);
            }
            offset(delta = -$eps)
                projection(cut = true)
                    translate([0, 0, (vario_rail_depth - vario_track_height)/2]);
            tearaway(vario_track_spacing);
            translate([0, vario_basic_length/3 + $eps, 0])
                mirror([0, 1])
                    tearaway(vario_track_spacing);
        }
    vario_track_profile();
    translate([vario_track_spacing*2, 0, 0])
        vario_track_profile(count = 2);
}