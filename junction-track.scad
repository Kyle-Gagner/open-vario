// Copyright © 2024 Kyle Gagner

include <open-vario.scad>

module vario_diverging_switch(
    count = 1, fraction = 2/3,
    number = 2, angle = 45,
    conn_north = 1, conn_south = -1, conn_northeast = 1,
    symmetric = false)
{
    l = fraction*vario_basic_length;
    r = vario_basic_radius + vario_track_spacing*(number + (count - 1)/2);
    r1 = r + vario_track_spacing/2;
    x0 = vario_track_spacing/2;
    y0 = l;
    x1 = (r - r1*cos(angle))*sign(angle);
    y1 = r1*abs(sin(angle));
    x2 = (r - r*cos(angle))*sign(angle);
    y2 = r*abs(sin(angle));
    x3 = r1 > l ? r - sqrt(r1^2 - l^2) : 0;
    y3 = l;
    x4 = x3 + (vario_track_spacing/2)*cos(angle)*sign(angle);
    y4 = y3 - (vario_track_spacing/2)*abs(sin(angle));
    difference()
    {
        union()
        {
            vario_straight_track(
                count = count, fraction = fraction,
                conn_north = conn_north, conn_south = conn_south,
                $vario_profile_positive = true, $vario_peg_positive = true);
            for (a = symmetric ? [-angle, angle] : [angle])
                vario_curve_track(
                    count = count, number = number, angle = a,
                    conn_north = conn_northeast, conn_south = conn_south,
                    $vario_profile_positive = true, $vario_peg_positive = true);
            if (x0 < x1)
                linear_extrude(vario_track_height, center = true)
                    if (y0 > y1)
                    {
                        if (symmetric)
                            polygon([
                                [0, 0], [-x2, y2], [-x1, y1], [-x0, y0],
                                [0, y0], [x0, y0], [x1, y1], [x2, y2]]);
                        else
                            polygon([[0, 0], [0, y0], [x0, y0], [x1, y1], [x2, y2]]);
                    }
                    else
                    {
                        if (symmetric)
                            polygon([[0, 0], [-x3 - 5, y3], [-x3, y3], [x3, y3], [x3 + 5, y3]]);
                        else
                            polygon([[0, 0], [0, y3], [x3, y3], [x3 + 5, y3]]);
                    }
        }
        vario_straight_track(
            count = count, fraction = fraction,
            conn_north = conn_north, conn_south = conn_south,
            extend = true, $vario_profile_negative = true, $vario_peg_negative = true);
        for (a = symmetric ? [-angle, angle] : [angle])
            vario_curve_track(
                count = count, number = number, angle = a,
                conn_north = conn_northeast, conn_south = conn_south,
                extend = true, $vario_profile_negative = true, $vario_peg_negative = true);
    }
}

module vario_parallel_switch(
    count = 1, fraction = 2/3,
    conn_north = 1, conn_south = -1, conn_northeast = 1,
    symmetric = false)
{
    l = is_undef(length) ? fraction*vario_basic_length : length;
    difference()
    {
        union()
        {
            vario_straight_track(
                count = count, fraction = fraction,
                conn_north = conn_north, conn_south = conn_south,
                $vario_profile_positive = true, $vario_peg_positive = true);
            for (shift = symmetric ? [-count, count] : [count])
                vario_sigmoid_track(
                    count = count, fraction = fraction, shift = shift,
                    conn_north = conn_northeast, conn_south = conn_south,
                    $vario_profile_positive = true, $vario_peg_positive = true);
            linear_extrude(vario_track_height, center = true)
                polygon([[0, 0], [symmetric ? -count*vario_track_spacing : 0, l], [count*vario_track_spacing, l]]); 
        }
        vario_straight_track(
            count = count, fraction = fraction,
            conn_north = conn_north, conn_south = conn_south,
            extend = true, $vario_profile_negative = true, $vario_peg_negative = true);
        for (shift = symmetric ? [-count, count] : [count])
            vario_sigmoid_track(
                count = count, fraction = fraction, shift = shift,
                conn_north = conn_northeast, conn_south = conn_south,
                extend = true, $vario_profile_negative = true, $vario_peg_negative = true);
    }
}

module vario_diverging_wye(
    count = 1,
    number1 = 0, angle1 = -45,
    number2 = 0, angle2 = 45,
    conn_south = -1, conn_northwest = 1, conn_northeast = 1)
{
    r11 = vario_basic_radius + vario_track_spacing*(number1 + (count - 1)/2);
    r21 = vario_basic_radius + vario_track_spacing*(number2 + (count - 1)/2);
    r12 = r11 + vario_track_spacing/2;
    r22 = r21 + vario_track_spacing/2;
    x11 = (r11 - r12*cos(angle1))*sign(angle1);
    y11 = r12*abs(sin(angle1));
    x12 = (r11 - r11*cos(angle1))*sign(angle1);
    y12 = r11*abs(sin(angle1));
    x21 = (r21 - r22*cos(angle2))*sign(angle2);
    y21 = r22*abs(sin(angle2));
    x22 = (r21 - r21*cos(angle2))*sign(angle2);
    y22 = r21*abs(sin(angle2));
    difference()
    {
        union()
        {
            vario_curve_track(
                count = count, number = number1, angle = angle1,
                conn_north = conn_northwest, conn_south = conn_south,
                $vario_profile_positive = true, $vario_peg_positive = true);
            vario_curve_track(
                count = count, number = number2, angle = angle2,
                conn_north = conn_northeast, conn_south = conn_south,
                $vario_profile_positive = true, $vario_peg_positive = true);
            if (sign(x11) != sign(x21))
                linear_extrude(vario_track_height, center = true)
                    polygon([[0, 0], [x12, y12], [x11, y11], [x21, y21], [x22, y22]]);
        }
        vario_curve_track(
            count = count, number = number1, angle = angle1,
            conn_north = conn_northwest, conn_south = conn_south,
            extend = true, $vario_profile_negative = true, $vario_peg_negative = true);
        vario_curve_track(
            count = count, number = number2, angle = angle2,
            conn_north = conn_northeast, conn_south = conn_south,
            extend = true, $vario_profile_negative = true, $vario_peg_negative = true);
    }
}

module vario_parallel_wye(
    count = 1, fraction = 2/3,
    conn_northwest = 1, conn_northeast = 1, conn_south = -1)
{
    l = is_undef(length) ? fraction*vario_basic_length : length;
    shift = count/2;
    difference()
    {
        union()
        {
            vario_sigmoid_track(
                count = count, fraction = fraction, shift = shift,
                conn_north = conn_northeast, conn_south = conn_south,
                $vario_profile_positive = true, $vario_peg_positive = true);
            vario_sigmoid_track(
                count = count, fraction = fraction, shift = -shift,
                conn_north = conn_northwest, conn_south = conn_south,
                $vario_profile_positive = true, $vario_peg_positive = true);
            linear_extrude(vario_track_height, center = true)
                polygon([[0, 0], [-vario_track_spacing*count/2, l], [vario_track_spacing*count/2, l]]); 
        }
        vario_sigmoid_track(
            count = count, fraction = fraction, shift = shift,
            conn_north = conn_northeast, conn_south = conn_south,
            extend = true, $vario_profile_negative = true, $vario_peg_negative = true);
        vario_sigmoid_track(
            count = count, fraction = fraction, shift = -shift,
            conn_north = conn_northwest, conn_south = conn_south,
            extend = true, $vario_profile_negative = true, $vario_peg_negative = true);
    }
}

module vario_level_junction(
    count = 1, fraction = 1/3, angle = 90,
    conn_north = 1, conn_east = 1, conn_south = -1, conn_west = -1)
{
    l = is_undef(length) ? fraction*vario_basic_length : length;
    difference()
    {
        union()
        {
            vario_straight_track(
                count = count, fraction = fraction,
                conn_north = conn_north, conn_south = conn_south,
                $vario_profile_positive = true, $vario_peg_positive = true);
            translate([0, l/2, 0])
                rotate([0, 0, -angle])
                    translate([0, -l/2, 0]) 
                        vario_straight_track(
                            count = count, fraction = fraction,
                            conn_north = conn_east, conn_south = conn_west,
                            $vario_profile_positive = true, $vario_peg_positive = true);
        }
        vario_straight_track(
            count = count, fraction = fraction,
            conn_north = conn_north, conn_south = conn_south,
            extend = true, $vario_profile_negative = true, $vario_peg_negative = true);
        translate([0, l/2, 0])
            rotate([0, 0, -angle])
                translate([0, -l/2, 0]) 
                    vario_straight_track(
                        count = count, fraction = fraction,
                        conn_north = conn_east, conn_south = conn_west,
                        extend = true, $vario_profile_negative = true, $vario_peg_negative = true);
    }
}

module vario_slip_junction(
    count = 1, fraction = 2/3, angle = 45, symmetric = true,
    conn_northwest = 1, conn_northeast = -1, conn_southeast = 1, conn_southwest = -1)
{
    l = is_undef(length) ? fraction*vario_basic_length : length;
    r = (vario_rail_centers/2)/(1/sin((180 - angle)/2) - 1);
    assert(r*cos((180 - angle)/2) <= l/2, "track length fraction too short for the given angle");
    translate([0, l/2, 0])
        difference()
        {
            union()
            {
                for (copy = [false, true])
                    rotate([0, 0, copy ? -angle : 0])
                        translate([0, -l/2, 0])
                            vario_straight_track(
                                count = count, fraction = fraction,
                                conn_north = copy ? conn_northeast: conn_northwest,
                                conn_south = copy ? conn_southwest : conn_southeast,
                                $vario_profile_positive = true, $vario_peg_positive = true);
                for (copy = symmetric ? [false, true] : [false])
                    rotate([0, 0, (copy ? 180 : 0)])
                        translate([0, -r*tan(angle/2), 0])
                            vario_curve_track(
                                count = count, angle = angle, radius = r,
                                conn_north = 0, conn_south = 0,
                                $vario_profile_positive = true, $vario_peg_positive = true);
            }
            for (copy = [false, true])
                rotate([0, 0, copy ? -angle : 0])
                    translate([0, -l/2, 0])
                        vario_straight_track(
                            count = count, fraction = fraction,
                            conn_north = copy ? conn_northeast: conn_northwest,
                            conn_south = copy ? conn_southwest : conn_southeast,
                            extend = true, $vario_profile_negative = true, $vario_peg_negative = true);
            for (copy = symmetric ? [false, true] : [false])
                rotate([0, 0, (copy ? 180 : 0)])
                    translate([0, -r*tan(angle/2), 0])
                        vario_curve_track(
                            count = count, angle = angle, radius = r,
                            conn_north = 0, conn_south = 0,
                            extend = true, $vario_profile_negative = true, $vario_peg_negative = true);
        }
}