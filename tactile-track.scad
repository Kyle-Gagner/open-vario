// Copyright © 2024 Kyle Gagner

include <open-vario.scad>

// a braille and nemeth code library is called for but out of scope
_braille_blank = "\u2800";
_nemeth_digits = [
    "\u2834", // 0
    "\u2802", // 1
    "\u2806", // 2
    "\u2812", // 3
    "\u2832", // 4
    "\u2822", // 5
    "\u2816", // 6
    "\u2836", // 7
    "\u2826", // 8
    "\u2814"]; // 9
_nemeth_numeric_indicator = "\u283C";
_nemeth_simple_fraction_open = "\u2839";
_nemeth_simple_fraction_close = "\u283C";
_nemeth_mixed_fraction_open = "\u2838\u2839";
_nemeth_mixed_fraction_close = "\u2838\u283C";
_nemeth_horizontal_fraction_line = "\u280C";

// UKAAF's "Standard dimensions for the UK Braille Cell" https://www.ukaaf.org/wp-content/uploads/2020/03/Braille-Standard-Dimensions.pdf
_ukaaf_braille_dot_dx = 2.5; // horizontal dot to dot spacing
_ukaaf_braille_dot_dy = 2.5; // vertical dot to dot spacing
_ukaaf_braille_cell_dx = 6; // horizontal cell to cell spacing (as measured to same location within each cell)
_ukaaf_braille_cell_dy = 10; // vertical cell to cell spacing (as measured to same location within each cell)
_ukaaf_braille_dot_height = 0.5; // height of dot
_ukaaf_braille_dot_diameter = 1.5; // diameter of base of dot
_ukaaf_braille_arc_radius = ((_ukaaf_braille_dot_diameter/2)^2 + _ukaaf_braille_dot_height^2)/(2*_ukaaf_braille_dot_height); // radius of dot's arc profile
_ukaaf_braille_arc_offset = _ukaaf_braille_arc_radius - _ukaaf_braille_dot_height; // offset of the center of the arc into the surface

// straight track with tactile elements, suitable for teaching fractions, supports twelfths
module vario_tactile_straight_track(fraction = 1/3, count = 1, conn_north = 1, conn_south = -1)
{
    frac12 = round(fraction*12)%12;
    denominator = (frac12 % 2 != 0 ? 2 : 1)*(frac12 % 4 != 0 ? 2 : 1)*(frac12 % 3 != 0 ? 3 : 1);
    numerator = round(fraction*denominator);
    fraction = numerator/denominator;
    l = vario_basic_length*fraction;
    w = vario_track_width + vario_track_spacing*(count - 1);
    nemeth_encoding = _nemeth_encode_fraction(numerator, denominator);
    vario_straight_track(fraction, count, conn_north, conn_south)
        if ($children > 0)
            children();
        else
            vario_track_profile(count);
    translate([w/2, 0, 0])
        rotate(120, [1, 1, 1])
        {
            translate([1.5*_ukaaf_braille_cell_dx, _ukaaf_braille_dot_dy, 0])
                _braille_lines([nemeth_encoding]);
            translate([l - 5, 0, 0])
                _typeset_fraction(numerator, denominator);
        }
    translate([-w/2, l, 0])
        rotate(120, [1, -1, -1])
        {
            translate([1.5*_ukaaf_braille_cell_dx, _ukaaf_braille_dot_dy, 0])
                _braille_lines([nemeth_encoding]);
            translate([l - 5, 0, 0])
                _typeset_fraction(numerator, denominator);
        }
}

module _typeset_fraction(numerator, denominator)
{
    quotient = floor(numerator / denominator);
    remainder = numerator % denominator;
    translate([0, 0, -0.6]) 
        minkowski()
        {
            linear_extrude($eps, center = true) 
            {
                if (quotient != 0)
                    translate([remainder == 0 ? 0 : -4.5, 0])
                        text(str(quotient), halign="center", valign="center", size = 8, font="Arial", $fn = 3);
                if (remainder != 0)
                {
                    translate([0, 0.7])
                        text(str(remainder), halign="center", valign="bottom", size = 4, font="Arial", $fn = 3);
                    translate([0, -0.7])
                        text(str(denominator), halign="center", valign="top", size = 4, font="Arial", $fn = 3);
                    translate([-2, -0.2])
                        square([4, 0.4]);
                }
            }
            cylinder(h = 1, r1 = 0.6, r2 = 0, $fn = 4);
        }
}

module _braille_lines(lines)
{
    $fn = 24;
    for (i = [0 : len(lines) - 1])
        for (j = [0 : len(lines[i]) - 1])
        let (sequence = ord(lines[i][j]) - ord(_braille_blank))
            for (k = [1 : 6])
                if (sequence % (2^k) >= 2^(k - 1))
                    translate([
                        j*_ukaaf_braille_cell_dx + (k > 3 ? _ukaaf_braille_dot_dx : 0),
                        -i*_ukaaf_braille_cell_dy - ((k - 1) % 3)*_ukaaf_braille_dot_dy, -_ukaaf_braille_arc_offset])
                        sphere(_ukaaf_braille_arc_radius);
}

function _nemeth_encode_integer(n) = n > 0 ?
    str(_nemeth_encode_integer((n - (n % 10))/10), _nemeth_digits[n % 10]) :
    "";

function _nemeth_encode_fraction(numerator, denominator) =
    let (
        quotient = floor(numerator / denominator),
        remainder = numerator % denominator)
        remainder == 0 ?
            str(_nemeth_numeric_indicator, _nemeth_encode_integer(quotient)) :
            str(
                _nemeth_simple_fraction_open,
                quotient == 0 ? "" : str(_nemeth_encode_integer(quotient), _nemeth_mixed_fraction_open),
                _nemeth_encode_integer(remainder),
                _nemeth_horizontal_fraction_line,
                _nemeth_encode_integer(denominator),
                quotient == 0 ? _nemeth_simple_fraction_close : _nemeth_mixed_fraction_close);