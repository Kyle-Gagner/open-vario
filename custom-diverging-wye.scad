// Copyright © 2024 Kyle Gagner

include <junction-track.scad>
include <quality.scad>

number1 = 0; // 1
angle1 = -45; // 15
number2 = 0; // 1
angle2 = 45; // 15
conn_northwest = 1; // [-1 : 1]
conn_northeast = 1; // [-1 : 1]
conn_south = -1; // [-1 : 1]
top_surface = "rails"; // ["rails", "road", "flat"]
bottom_surface = "rails"; // ["rails", "road", "flat"]
count = 1; // [1 : 2]

vario_diverging_wye(
    count = count,
    number1 = number1,
    angle1 = angle1,
    number2 = number2,
    angle2 = angle2,
    conn_northwest = conn_northwest,
    conn_northeast = conn_northeast,
    conn_south = conn_south,
    $vario_profile_top_rails = top_surface == "rails",
    $vario_profile_bottom_rails = bottom_surface == "rails",
    $vario_profile_top_road = top_surface == "road",
    $vario_profile_bottom_road = bottom_surface == "road");