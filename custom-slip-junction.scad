// Copyright © 2024 Kyle Gagner

include <junction-track.scad>
include <quality.scad>

fraction = 0.66667; // [0.5 : 0.083333 : 1]
angle = 45; // 15
conn_northwest = 1; // [-1 : 1]
conn_northeast = -1; // [-1 : 1]
conn_southeast = 1; // [-1 : 1]
conn_southwest = -1; // [-1 : 1]
top_surface = "rails"; // ["rails", "road", "flat"]
bottom_surface = "rails"; // ["rails", "road", "flat"]
count = 1; // [1 : 2]
symmetric = true;

vario_slip_junction(
    fraction = fraction,
    count = count,
    angle = angle,
    conn_northwest = conn_northwest,
    conn_northeast = conn_northeast,
    conn_southeast = conn_southeast,
    conn_southwest = conn_southwest,
    symmetric = symmetric,
    $vario_profile_top_rails = top_surface == "rails",
    $vario_profile_bottom_rails = bottom_surface == "rails",
    $vario_profile_top_road = top_surface == "road",
    $vario_profile_bottom_road = bottom_surface == "road");